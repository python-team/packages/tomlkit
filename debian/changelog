python-tomlkit (0.13.2-1) unstable; urgency=medium

  * New upstream version.

 -- Emmanuel Arias <eamanu@debian.org>  Thu, 22 Aug 2024 06:40:46 -0300

python-tomlkit (0.13.0-1) unstable; urgency=medium

  * New upstream version.

 -- Emmanuel Arias <eamanu@debian.org>  Fri, 09 Aug 2024 21:20:49 -0300

python-tomlkit (0.12.5-1) unstable; urgency=medium

  [ Scott Kitterman ]
  * Remove myself from uploaders

  [ Emmanuel Arias ]
  * Add my self as Uploader.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Set field Upstream-Name in debian/copyright (routine-update)
  * d/upstream: Add metadata file.

 -- Emmanuel Arias <eamanu@debian.org>  Fri, 17 May 2024 21:28:45 -0300

python-tomlkit (0.12.4-1) unstable; urgency=medium

  * Add Rules-Requires-Root no
  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Sat, 02 Mar 2024 10:42:57 -0500

python-tomlkit (0.12.3-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Fri, 29 Dec 2023 14:14:18 -0500

python-tomlkit (0.12.1-1) unstable; urgency=medium

  * Upload to unstable

 -- Scott Kitterman <scott@kitterman.com>  Tue, 01 Aug 2023 13:42:42 -0400

python-tomlkit (0.12.1-1~exp1) experimental; urgency=medium

  * New upstream release
    - Refresh/update patches

 -- Scott Kitterman <scott@kitterman.com>  Mon, 31 Jul 2023 12:34:21 -0400

python-tomlkit (0.11.8-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 13 Jun 2023 00:29:54 -0400

python-tomlkit (0.11.7-1) unstable; urgency=medium

  * New upstream release
  * Bump standards-version to 4.6.2 without further change

 -- Scott Kitterman <scott@kitterman.com>  Sat, 01 Apr 2023 16:34:39 -0400

python-tomlkit (0.11.6-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Tue, 01 Nov 2022 00:53:32 -0400

python-tomlkit (0.11.5-1) unstable; urgency=medium

  * New upstream release
  * Delete d/patches/0001-add-missing-docs-in-README.patch, file no longer
    missing
  * Bump standards-version to 4.6.1 without further change
  * Build and install text documentation for docs folder
  * Add d/p/0001-Patch-README.md-to-point-to-local-documentation-inst.patch

 -- Scott Kitterman <scott@kitterman.com>  Fri, 14 Oct 2022 19:02:54 -0400

python-tomlkit (0.9.2-1) unstable; urgency=medium

  * New upstream release
  * Switch to pyproject.toml based build
    - Add python3-poetry-core and pybuild-plugin-pyproject to build-depends
    - Remove python3-setuptools from build-depends
  * Update d/watch to version 4
  * Update d/copyright
  * Add d/patches/0001-add-missing-docs-in-README.patch to restore usage
    documentation that was moved to a new docs directory which was then left
    out of the source tarball

 -- Scott Kitterman <scott@kitterman.com>  Tue, 08 Feb 2022 09:18:13 -0500

python-tomlkit (0.8.0-1) unstable; urgency=medium

  * New upstream release

 -- Scott Kitterman <scott@kitterman.com>  Thu, 23 Dec 2021 00:35:05 -0500

python-tomlkit (0.7.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Scott Kitterman ]
  * New upstream release
  * Add python3-yaml to build-depends and test depends for test execution
  * Update homepage to point to github instead of pypi
  * Bump standards-version to 4.6.0 without further change

 -- Scott Kitterman <scott@kitterman.com>  Thu, 25 Nov 2021 03:47:16 -0500

python-tomlkit (0.6.0-2) unstable; urgency=medium

  * Source only upload to allow Testing migration

 -- Scott Kitterman <scott@kitterman.com>  Thu, 25 Jun 2020 08:07:08 -0400

python-tomlkit (0.6.0-1) unstable; urgency=medium

  * Initial release (Closes: #959797)

 -- Scott Kitterman <scott@kitterman.com>  Tue, 05 May 2020 11:18:19 -0400
